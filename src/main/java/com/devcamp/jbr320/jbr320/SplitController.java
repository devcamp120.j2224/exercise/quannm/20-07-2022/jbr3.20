package com.devcamp.jbr320.jbr320;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitController {

    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String[]> getSpilt() {
        ArrayList<String[]> list = new ArrayList<>();

        String inp = "a b c d e";
        String[] acc = inp.split(" ", 5);

        list.add(acc);
        
        return list;
    }
}
