package com.devcamp.jbr320.jbr320;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr320Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr320Application.class, args);
	}

}
